﻿/*GR-KURUMI Sketch Template Version: V1.13*/
#include <Arduino.h>
#include <Wire.h>

#define SLAVE_ADDRESS 0x10 //Arduino pro mini's address

#define RGB                 4 // brightness + RGB color num
#define DOUBLE_COLOR_FORMAT 9+1 //HEADER+PATTERN+BRGB+BRGB

// Pin 22,23,24 are assigned to RGB LEDs.
int led_red   = 22; // LOW active
int led_green = 23; // LOW active
int led_blue  = 24; // LOW active

byte old_color[DOUBLE_COLOR_FORMAT] = {0};
byte cur_color[DOUBLE_COLOR_FORMAT] = {0};

void changeRGB();
boolean checkRGB();

// the setup routine runs once when you press reset:
void setup() {
	//setPowerManagementMode(PM_STOP_MODE, 0, 1023); //Set CPU STOP_MODE in delay()
	//setOperationClockMode(CLK_LOW_SPEED_MODE); //Set CPU clock from 32MHz to 32.768kHz

	// initialize the digital pin as an output.
	Serial.begin(115200);
	Serial2.begin(115200);
	Wire.begin();
  
	pinMode(led_red, OUTPUT);
	pinMode(led_green, OUTPUT);
	pinMode(led_blue, OUTPUT);

	// turn the LEDs on, glow white.
	digitalWrite(led_red, HIGH);
	digitalWrite(led_green, HIGH);
	digitalWrite(led_blue, LOW);
	
	for(int i=0; i<DOUBLE_COLOR_FORMAT; i++) {
		old_color[i] = 0;
		cur_color[i] = 0;
	}
}

// the loop routine runs over and over again forever:
void loop() {

	if(Serial2.available()>DOUBLE_COLOR_FORMAT) {
		byte temp = Serial2.read();
		if(temp == 0xFF) {
			for(int i=0; i<DOUBLE_COLOR_FORMAT; i++) {
//				byte temp = Serial2.read();
				cur_color[i] = Serial2.read();
				Serial.print(i);
				Serial.print(":");
				Serial.println(cur_color[i]);
//				Serial.println(temp);

			}
			changeRGB();
		}
	}
}

// send RGB data to change LED color to Arduino mini

void changeRGB() {
    byte header = 0xFF;
    
	Wire.beginTransmission(SLAVE_ADDRESS);
	Wire.write(header);
	for(int i=0; i<DOUBLE_COLOR_FORMAT; i++) {
		Wire.write((byte)cur_color[i]);
		old_color[i] = cur_color[i];
//				Serial.print(i);
//				Serial.print(":");
//				Serial.println(cur_color[i]);
		
	}
	Wire.endTransmission();
}

// check if the present color is the same as old.
boolean checkRGB() {
	boolean flag = false;

	for(int i=0; i<DOUBLE_COLOR_FORMAT; i++) {
		if(old_color[i] != cur_color[i]) {
			flag = true;
		}
	}
	
	return flag;
}


