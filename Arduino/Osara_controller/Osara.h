
#ifndef OSARA_H
#define OSARA_H

#include <Adafruit_NeoPixel.h>
#include <Arduino.h>

using namespace std;

#ifdef __AVR__
  #include <avr/power.h>
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            6

// color num
#define RGB            4 // Birghtness[0] + RGB
#define DOUBLE_COLOR_FORMAT 9+1 //HEADER+PATTERN+BRGB+BRGB

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      40

// gradation phase
#define PHASE          200.0


class Osara{
private:
  byte mOldSingleColor[RGB];
  byte mCurSingleColor[RGB];
  byte mOldMultiColor[NUMPIXELS][RGB];
  byte mCurMultiColor[NUMPIXELS][RGB];
  Adafruit_NeoPixel pixels;
  boolean mFlag;
//  byte diff[NUMPIXELS][RGB];               // valiable for gradual change
  
public:
  Osara();
  ~Osara();
  void changeColorGradually();  
  void setCurSingleColor(byte x[]);
  void setOldSingleColor(byte x[]);
  void setCurMultiColor(byte x[NUMPIXELS][RGB]);
  void setOldMultiColor(byte x[NUMPIXELS][RGB]);

  void setCurSingleColor(byte value, int num);
  void setOldSingleColor(byte value, int num);

  byte *getOldSingleColorPtr();
  byte *getCurSingleColorPtr(); 
  boolean getFlag();
  boolean setFlag(boolean TorF);
  void set2ColorPattern(byte color1[], byte color2[], int num1, int num2);
  void set2ColorAlternatePattern(byte color1[], byte color2[], int num1, int num2);
  void set2ColorGradation(byte color1[], byte color2[]);
  void set2ColorGradualPattern(byte color1[], byte color2[]);
  void changeColorGradually2();
  void partyRandom();
  void partyFlash();
};

#endif
