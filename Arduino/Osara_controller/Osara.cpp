
#include "Osara.h"

using namespace std;

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.

Osara::Osara(): pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800) {

  for (int i = 0; i < RGB; i++) {
    mOldSingleColor[i] = 0;
    mCurSingleColor[i] = 0;
    for (int j = 0; j < NUMPIXELS; j++) {
      mOldMultiColor[j][i] = 0;
      mCurMultiColor[j][i] = 0;
    }
  }  
  randomSeed(analogRead(0));


  pixels.begin(); // This initializes the NeoPixel library.
}

Osara::~Osara() {

}

void Osara::changeColorGradually() {
  float diff[RGB] = {0};
  float temp_color[RGB] = {0};

  for (int i = 0; i < RGB; i++) {
    Serial.print(mCurSingleColor[i]);
    Serial.print(":");
  }
  Serial.println("");

  for (int i = 0; i < RGB; i++) {
    diff[i] = (float)((mCurSingleColor[i] - mOldSingleColor[i]) / PHASE);
    temp_color[i] = mOldSingleColor[i];
  }

  for (int j = 0; j < PHASE; j++) {
    for (int i = 0; i < RGB; i++) {
      temp_color[i] += diff[i];
    }
    for (int i = 0; i < NUMPIXELS; i++) {
      pixels.setBrightness((byte)temp_color[0]);
      // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
      pixels.setPixelColor(i, pixels.Color((byte)temp_color[1], (byte)temp_color[2], (byte)temp_color[3])); // Moderately bright green color.
    }
    pixels.show(); // This sends the updated pixel color to the hardware.
    delay(1);
  }
  memcpy(mOldSingleColor, mCurSingleColor, RGB);
}

void Osara::setCurSingleColor(byte x[]) {
  for (int i = 0; i < sizeof(x) / sizeof(x[0]); i++) {
    mCurSingleColor[i] = x[i];
  }
}

void Osara::setOldSingleColor(byte x[]) {
  for (int i = 0; i < sizeof(x) / sizeof(x[0]); i++) {
    mOldSingleColor[i] = x[i];
  }
}

void Osara::setCurMultiColor(byte x[NUMPIXELS][RGB]) {
  for (int j = 0; j < NUMPIXELS; j++) {
    for (int i = 0; i < RGB; i++) {
      mCurMultiColor[j][i];
    }
  }
}

void Osara::setOldMultiColor(byte x[NUMPIXELS][RGB]) {
  for (int j = 0; j < NUMPIXELS; j++) {
    for (int i = 0; i < RGB; i++) {
      mOldMultiColor[j][i];
    }
  }
}


void Osara::setCurSingleColor(byte value, int num) {
  mCurSingleColor[num] = value;
}

void Osara::setOldSingleColor(byte value, int num) {
  mOldSingleColor[num] = value;
}



byte *Osara::getOldSingleColorPtr() {
  return mOldSingleColor;
}

byte *Osara::getCurSingleColorPtr() {
  return mCurSingleColor;
}

boolean Osara::getFlag() {
  return mFlag;
}

boolean Osara::setFlag(boolean TorF) {
  mFlag = TorF;
}


void Osara::set2ColorPattern(byte color1[], byte color2[], int num1, int num2) {
  int color = 0;
  int turn = num1 + num2;
  byte temp_color[NUMPIXELS][RGB] = {0};

  for (int i = 0; i < NUMPIXELS; i++) {
    if (i % turn == 0) {
      color = 0;
    }
    else if (i % turn > num1 - 1 ) {
      color = 1;
    }

    switch (color) {
      case 0:
        for (int j = 0; j < RGB; j++) {
          temp_color[i][j] = color1[j];
        }
        break;

      case 1:
        for (int j = 0; j < RGB; j++) {
          temp_color[i][j] = color2[j];
        }
        break;
    }
  }

  for (int i = 0; i < NUMPIXELS; i++) {
    for (int j = 0; j < RGB; j++) {
      mCurMultiColor[i][j] = (byte)temp_color[i][j];
    }
  }

  Serial.println("temp color");
  for (int j = 0; j < NUMPIXELS; j++) {
    for (int i = 0; i < RGB; i++) {
      Serial.print(mCurMultiColor[j][i]);
      Serial.print(":");
    }
    Serial.println("");
  }
  changeColorGradually2();
}

void Osara::set2ColorGradation(byte color1[], byte color2[]) {
  float diff[RGB] = {0};
  float temp_color[NUMPIXELS][RGB] = {0};

  for (int i = 0; i < RGB; i++) {
    diff[i] = (float)(2.0 * (float)((float)((float)color2[i] - (float)color1[i]) / (float)NUMPIXELS));
    Serial.print(diff[i]);
    Serial.println(":");
    for (int j = 0; j < NUMPIXELS; j++) {
      temp_color[j][i] = color1[i];
    }
  }

  for (int i = 0; i < NUMPIXELS; i++) {
    if (i < (int)(NUMPIXELS / 2.0) && i != 0 ) {
      for (int j = 0; j < RGB; j++) {
        temp_color[i][j] = temp_color[i - 1][j] + diff[j];
        Serial.print((byte)temp_color[i][j]);
        Serial.print(":");
      }
      Serial.println("");
    }
    else if (i != 0) {
      for (int j = 0; j < RGB; j++) {
        temp_color[i][j] = temp_color[i - 1][j]  - diff[j];
        Serial.print((byte)temp_color[i][j]);
        Serial.print(":");
      }
      Serial.println("");
    }
    else {
    }
  }

  for (int i = 0; i < NUMPIXELS; i++) {
    for (int j = 0; j < RGB; j++) {
      mCurMultiColor[i][j] = (byte)temp_color[i][j];
      Serial.print(mCurMultiColor[i][j]);
      Serial.print(":");
    }
    Serial.println("");
  }
  changeColorGradually2();
}


void Osara::changeColorGradually2() {
  //  byte diff[NUMPIXELS][RGB];               // valiable for gradual change
  float temp_color[NUMPIXELS][RGB] = {0};

  for (int i = 0; i < NUMPIXELS; i++) {
    for (int j = 0; j < RGB; j++) {
      temp_color[i][j] = mOldMultiColor[i][j];
    }
  }

  for (int i = 0; i < PHASE; i++) {
    for (int j = 0; j < NUMPIXELS; j++) {
      for (int k = 0; k < RGB; k++) {
        temp_color[j][k] += (float)((float)(mCurMultiColor[j][k] - mOldMultiColor[j][k]) / PHASE);
      }

      pixels.setBrightness((byte)temp_color[j][0]);
      pixels.setPixelColor(j, pixels.Color((byte)temp_color[j][1], (byte)temp_color[j][2], (byte)temp_color[j][3]));

    }
    pixels.show();
    delayMicroseconds(1);
  }

  //copy current color array to old color array
  for (int i = 0; i < NUMPIXELS; i++) {
    for (int j = 0; j < RGB; j++) {
      mOldMultiColor[i][j] = mCurMultiColor[i][j];
    }
  }
}

void Osara::partyRandom()
{
  long randomNum[4] = {0}; // pos, RGB
  randomNum[0] = random(1, NUMPIXELS - 1);

/*
  for (int i = 0; i < 3; i++) {
    randomNum[i + 1] = random(1, 255);
  }
*/

 randomNum[1] = random(100, 255);
 randomNum[2] = random(1, 150);
 randomNum[3] = random(1, 200);

  for (int i = 0; i < NUMPIXELS; i++) {
    if (!(((int)randomNum[0] - 1) <= i && i <= ((int)randomNum[0] + 1))) {
      pixels.setBrightness(240);
      pixels.setPixelColor(i, pixels.Color((byte)0x01, (byte)0x01, (byte)0x01));
    }
    else {
      pixels.setBrightness(240);
      pixels.setPixelColor(i, pixels.Color((byte)randomNum[1], (byte)randomNum[2], (byte)randomNum[3]));
    }
  }
  pixels.show();
  delay(10);
}

void Osara::partyFlash() {
  long randomNum[3] = {0}; //RGB
  float fRandomNum[3] = {0.0f};
  randomSeed(analogRead(0));

  float stepNum = 30.0f; 

  for (int i = 0; i < 3; i++) {
    randomNum[i] = random(1, 255);
    fRandomNum[i] = (float)randomNum[i]/(pow(stepNum,2.0));
  }

  for (int j=2; j<(int)stepNum; j++) {
    for (int i = 0; i < NUMPIXELS; i++) {
      pixels.setBrightness(240);
      pixels.setPixelColor(i, pixels.Color((byte)(fRandomNum[0]*pow(j,2.0)), (byte)(fRandomNum[1]*pow(j,2.0)), (byte)(fRandomNum[2]*pow(j,2.0))));
    }
    pixels.show();
    delay((int)(1.0f/stepNum));
  }

  for (int j=(int)stepNum; 2<j; j--) {
    for (int i = 0; i < NUMPIXELS; i++) {
      pixels.setBrightness(240);
      pixels.setPixelColor(i, pixels.Color((byte)(fRandomNum[0]*pow(j,2.0)), (byte)(fRandomNum[1]*pow(j,2.0)), (byte)(fRandomNum[2]*pow(j,2.0))));
    }
    pixels.show();
    delay((int)(1.0f/stepNum));
  }
}

