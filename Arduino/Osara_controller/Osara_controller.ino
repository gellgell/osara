/// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Wire.h>
#include "Osara.h"

#define SLAVE_ADDRESS 0x10

#define DCDC_SWITCH 9

#define BLE

byte colorNum = 0;
byte colorPat = 0;

Osara osara;

void setup() {
  Serial.begin(115200);
//  pinMode(DCDC_SWITCH, OUTPUT);
//  digitalWrite(DCDC_SWITCH, LOW);
  
  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(twiCallBack);
}

void loop() {
// For a set of NeoPixels the first NeoPixel is 0, second is 1, all the way up to the count of pixels minus one.]
#ifdef BLE  
  if(osara.getFlag()){
    digitalWrite(DCDC_SWITCH, HIGH);
    if(colorNum == 0xFE) {
//      osara.changeColorGradually();    
//      osara.set2ColorPattern();
      osara.set2ColorGradation(osara.getCurSingleColorPtr(), osara.getCurSingleColorPtr());
    }
    else if(colorNum == 0xFD) {
      switch(colorPat) {
        case 0:
          osara.set2ColorGradation(osara.getOldSingleColorPtr(), osara.getCurSingleColorPtr());
          break;

        case 2:
          osara.set2ColorPattern(osara.getOldSingleColorPtr(), osara.getCurSingleColorPtr(), 2, 2);    
          break;

        case 4:
          osara.set2ColorPattern(osara.getOldSingleColorPtr(), osara.getCurSingleColorPtr(), 4, 4);    
          break;

        case 5:
          osara.partyRandom();  
          break;

        case 6:
          osara.partyFlash();
          break;
      }
    }
    osara.setFlag(false);
  }

  if(colorPat==5) {
    osara.partyRandom();       
  }
  else if(colorPat==6) {
    osara.partyFlash();
  }
#endif
}

void twiCallBack(int a) {
  noInterrupts();
  if (Wire.available() > DOUBLE_COLOR_FORMAT) {
    byte temp = (byte)Wire.read();
    if (temp == 0xFF) {
      for (int i = 0; i < DOUBLE_COLOR_FORMAT; i++) {
        byte data = (byte)Wire.read();
        switch(i) {
          case 0:
            colorNum = data;
            break;

          case 1:
            colorPat = data;
            break;
  
          case 2:
          case 3:
          case 4:
          case 5:
            osara.setCurSingleColor(data, i-2);
            break;
            
            case 6:
            case 7:
            case 8:
            case 9:
              //if change for single color, this phase is skipped 
              if(colorNum != 0xFE) {
                osara.setOldSingleColor(data, i-6);          
              }
              break;

          default:
            break;
        }
#ifdef _DEBUG
        Serial.print(i);
        Serial.print(":");
        Serial.println(data,HEX);
#endif //_DEBUG
      }
      osara.setFlag(true);
    }
  }
  interrupts();
}










