#include "mbed.h"
#include "ble/BLE.h"
#include "ble/services/BatteryService.h"
#include "ble/services/DeviceInformationService.h"

#define NEED_CONSOLE_OUTPUT 1 /* Set this if you need debug messages on the console;
                               * it will have an impact on code-size and power consumption. */
// pin assign
#define UART_RX p16
#define UART_TX p19

#define RGB                 4 //BRIGHTNESS + RGB
#define DOUBLE_COLOR_FORMAT 9 //PATTERN+BRGB+BRGB

Serial  serial(UART_TX, UART_RX);

const static uint8_t  DEVICE_NAME[] = "Osara";
static volatile bool  triggerSensorPolling = false;

BLEDevice  ble;

const uint8_t UUID_OSARA_SERVICE[] = {0xcf,0xc2,0x14,0xed,0xb6,0x83,0x4a,0xe1,0x8b,0x22,0x2c,0x2c,0x71,0x82,0x11,0xee};
const uint8_t UUID_SINGLE_COLOR_CHAR[] = {0xbb,0x07,0xce,0x17,0x51,0x9b,0x48,0x29,0x9a,0x82,0xdc,0x83,0x0e,0x36,0x88,0x57};
const uint8_t UUID_DOUBLE_COLOR_CHAR[] = {0x29,0x41,0x8e,0xf1,0x1e,0x98,0x4a,0xe1,0x81,0xd0,0x99,0xa1,0xca,0xb4,0x31,0x48};

uint8_t rgb_color_char[RGB] = {0};
uint8_t drgb_color_char[DOUBLE_COLOR_FORMAT] = {0};

GattCharacteristic Color_Char(UUID_SINGLE_COLOR_CHAR, rgb_color_char,
                            sizeof(rgb_color_char),
                            sizeof(rgb_color_char),
                            GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_WRITE
                        |   GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_READ);

// for pattern double color
GattCharacteristic DColor_Char(UUID_DOUBLE_COLOR_CHAR, drgb_color_char,
                            sizeof(drgb_color_char),
                            sizeof(drgb_color_char),
                            GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_WRITE
                        |   GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_READ);


GattCharacteristic *OSARA_chars[] = {&Color_Char, &DColor_Char};

GattService Osara_Service = GattService(UUID_OSARA_SERVICE,
                            OSARA_chars,
                            sizeof(OSARA_chars)/sizeof(GattCharacteristic *));
/* Battery Level Service */

#ifdef _BATTERY
uint8_t            batt = 100;     /* Battery level */
uint8_t            read_batt = 0;  /* Variable to hold battery level reads */

GattCharacteristic battLevel ( GattCharacteristic::UUID_BATTERY_LEVEL_CHAR,     
                                 (uint8_t *)batt, 1, 1,
                                 GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_NOTIFY);

GattCharacteristic *battChars[] = {&battLevel};
GattService        battService(GattService::UUID_BATTERY_SERVICE, battChars,
                                sizeof(battChars) / sizeof(GattCharacteristic *));

uint16_t             uuid16_list[] = {GattService::UUID_BATTERY_SERVICE};
#endif //_BATTERY

static Gap::ConnectionParams_t connectionParams;

void disconnectionCallback(const Gap::DisconnectionCallbackParams_t *params)    // Mod
{
    
//    DEBUG("Disconnected handle %u, reason %u\r\n", params->handle, params->reason);
//    DEBUG("Restarting the advertising process\r\n");
    ble.gap().startAdvertising();
}

void onConnectionCallback(const Gap::ConnectionCallbackParams_t *params)   //Mod
{
    
}

void periodicCallback(void)
{

    /* Note that the periodicCallback() executes in interrupt context, so it is safer to do
     * heavy-weight sensor polling from the main thread. */
    triggerSensorPolling = true;
}

void DataWrittenCallback(const GattWriteCallbackParams *params)
{
    if (params->handle == Color_Char.getValueAttribute().getHandle() && (params->len == RGB)) {
        memcpy(rgb_color_char, params->data, params->len);
        ble.gattServer().write(Color_Char.getValueHandle(), rgb_color_char, RGB);
        
        serial.putc(0xFF);
        serial.putc(0xFE);        
        serial.putc(0x00);

        for(int i=0; i<RGB; i++) {            
            if(rgb_color_char[i] == 0xFF || rgb_color_char[i] == 0xFE || rgb_color_char[i] == 0xFD) {
                rgb_color_char[i] = 0xFC;
            }
            serial.putc(rgb_color_char[i]);
        }
        // dummy data for keeping the consistency of protocol
        serial.putc(0x00);
        serial.putc(0x00);
        serial.putc(0x00);
        serial.putc(0x00);
        
        return;
    }

    if (params->handle == DColor_Char.getValueAttribute().getHandle() && (params->len == DOUBLE_COLOR_FORMAT)) {
        memcpy(drgb_color_char, params->data, params->len);
        ble.gattServer().write(DColor_Char.getValueHandle(), drgb_color_char, DOUBLE_COLOR_FORMAT);
        
        serial.putc(0xFF);
        serial.putc(0xFD);
        for(int i=0; i<DOUBLE_COLOR_FORMAT; i++) {            
            if(drgb_color_char[i] == 0xFF || drgb_color_char[i] == 0xFE || drgb_color_char[i] == 0xFD) {
                drgb_color_char[i] = 0xFC;
            }
            serial.putc(drgb_color_char[i]);
        }
        return;
    }
}

/**************************************************************************/
/*!
    @brief  Program entry point
*/
/**************************************************************************/

int main(void)
{
    
    /* Setup blinky led */
    serial.baud(115200);
    serial.format(8, Serial::None, 1);
//    ticker.attach(periodicCallback, 1);
       
//    DEBUG("Initialising the nRF51822\r\n");
    ble.init();
//    DEBUG("Init done\r\n");
    ble.gap().onDisconnection(disconnectionCallback);
    ble.gap().onConnection(onConnectionCallback);
    ble.gattServer().onDataWritten(DataWrittenCallback);

    ble.gap().getPreferredConnectionParams(&connectionParams);

    /* setup advertising */
    ble.gap().setDeviceName(DEVICE_NAME);
    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::BREDR_NOT_SUPPORTED | GapAdvertisingData::LE_GENERAL_DISCOVERABLE);
#ifdef _BATTERY
    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::COMPLETE_LIST_16BIT_SERVICE_IDS, (uint8_t*)uuid16_list, sizeof(uuid16_list));
#endif
    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::GENERIC_THERMOMETER);
    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::COMPLETE_LOCAL_NAME, (uint8_t *)DEVICE_NAME, sizeof(DEVICE_NAME));
    
    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::COMPLETE_LIST_128BIT_SERVICE_IDS, (const uint8_t *)UUID_OSARA_SERVICE, sizeof(UUID_OSARA_SERVICE));
    
    ble.gap().setAdvertisingType(GapAdvertisingParams::ADV_CONNECTABLE_UNDIRECTED);
    ble.gap().setAdvertisingInterval(160); /* 100ms; in multiples of 0.625ms. */
    ble.gap().startAdvertising();
//    DEBUG("Start Advertising\r\n");
#ifdef _BATTERY
    ble.gattServer().addService(battService);
#endif
//    DEBUG("Add Service\r\n");

    ble.addService(Osara_Service);

    while (true) {
        //0.0006-0.0015
        if (triggerSensorPolling) {
        } else {
            ble.waitForEvent();
        }
    }
}



