# Osara #
Osaraは、色彩で料理の印象を変え、より美しく見せたり、それを食べる人の食欲を抑制したりすることができる新しいお皿です

![main.png](https://bitbucket.org/repo/eqedy5/images/3458977252-main.png)

## Overview ##
Osara本体を動かすために必要な組み込みソフトがまとめられています．
Arduino/Osara_controller-Arduino Pro MiniでNeoPixelRGB RINGを制御するためのプログラム
 制御にはAdafruitのAdafruit_NeoPixelライブラリを利用しています．
 https://github.com/adafruit/Adafruit_NeoPixel
 
gr-kurumi-GR-KURUMIでLED制御用Arduinoを制御するプログラム
nRF51822-BVMCN5103-BKでiOSアプリと通信するためのmbedプログラム

## Features ##
iOSアプリをインストールすることでBLEによる無線通信機能や
写真撮影により推薦される色やパターンの選択（開発中）が利用可能になります．
https://github.com/koooootake/Osara-iOS

筐体データはこちらよりダウンロードが可能です．(Fusion 360用データ)
http://a360.co/2eTuTFt

## Requirement ##
- Arduino IDE 1.6.11
- Renesus Account (Renesas webコンパイラを使えること)
   http://gadget.renesas.com/ja/
- mbed account (mbed webコンパイラを使えること)
   https://developer.mbed.org/

## Movie ##
YouTube:https://www.youtube.com/watch?v=HTnP-cWBjqw&feature=youtu.be